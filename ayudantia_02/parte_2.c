#include <stdio.h>
#include <stdlib.h>

void funcion(int arreglo[]) {
  //  arreglo[] = {4, 2, 3};
  arreglo[0] = 4;
  arreglo[1] = 2;
  arreglo[2] = 3;
}

int *funcion2(void) {
  int *arreglo;
  arreglo = (int *)malloc(3 * sizeof(int));
  arreglo[0] = 40;
  arreglo[1] = 20;
  arreglo[2] = 30;
  return arreglo;
}

int main(int argc, char *argv[]) {
  int ar[3];
  int *ar2;

  funcion(ar);

  for (int i = 0; i < 3; i++) {
    printf("%i ", ar[i]);
  }
  printf("\n");

  ar2 = funcion2();
  for (int i = 0; i < 3; i++) {
    printf("%i ", ar2[i]);
  }
  printf("\n");

  free(ar2);
  for (int i = 0; i < 3; i++) {
    printf("%i ", ar2[i]);
  }
  printf("\n");
  return 0;
}