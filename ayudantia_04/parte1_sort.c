#include "parte1.h"

void sort(int *arr, int N) {
  int comparaciones = 0;
  int asignaciones = 0;
  for (int i = 0; i < N; i++) {
    for (int j = 0; j < N - 1 - i; j++) {
      comparaciones++;
      if (arr[j] > arr[j + 1]) {
        int aux = arr[j];
        arr[j] = arr[j + 1];
        arr[j + 1] = aux;
        asignaciones += 3;
      }
    }
  }
  printf("\n\nComparaciones: %i\nAsignaciones. %i\n\n", comparaciones,
         asignaciones);
}