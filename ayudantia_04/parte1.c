#include "parte1.h"

int main(int argc, char *argv[]) {
  int arreglo[] = {1, 5, 3, 6, 8, 3, 2, 5, 4, 3, 123, 4, 3, 2, 4};
  int N = 15;

  print_arreglo(arreglo, N);
  sort(arreglo, N);
  print_arreglo(arreglo, N);

  return 0;
}