#include <stdio.h>
#include <stdlib.h>

int *f1(void) {
  int *y;
  y = (int *)malloc(8 * sizeof(int));
  int x[8] = {5, 6, 7, 8, 9, 1, 2, 3};
  return x;
}

int main(int argc, char *argv[]) {
  int *z;

  z = f1();

  for (int i = 0; i < 8; i++) {
    z[i] = i * 10;
  }

  for (int i = 0; i < 8; i++) {
    printf("%i ", z[i]);
  }
  printf("\n");

  return 0;
}