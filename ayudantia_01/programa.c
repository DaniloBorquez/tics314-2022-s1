#include <stdio.h>
int doble_suma(int a, int b);
int suma(int a, int b);
int resta(int a, int b);

void cambio(int *a);

int main(void) {
  printf("%i\n", suma(3, 4));
  printf("%i\n", resta(3, 4));

  int b = 3;
  printf("en el main: %i\n", b);
  cambio(&b);

  printf("en el main: %i\n", b);

  return 0;
}

int doble_suma(int a, int b) { return suma(a, b) + suma(a, b); }
int suma(int a, int b) { return a + b; }
int resta(int a, int b) { return a - b; }

void cambio(int *a) {
  *a = 5;
  printf("Dentro %i\n", *a);
}