#include <stdio.h>

#include "funciones.h"

int main(void) {
  printf("hola mundo\n");

  printf("%i\n", suma(4, 5));

  int resultado = suma(5, 6);

  printf("%i\n", resultado);
  return 0;
}