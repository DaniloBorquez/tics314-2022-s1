#include <stdio.h>

int main(int argc, char *argv[]) {
  int x = 0;

  while (x < 5) {
    printf("valor de x: %i\n", x);
    x++;
    if (x == 2) {
      break;
    }
  }

  printf("\n");

  for (int i = 0; i < 5; i++) {
    printf("valor de i: %i\n", i);
  }
  return 0;
}