# Contenido de los archivos

Brevísimo resumen de lo que contiene cada archivo :relieved:

| Archivo  | Descripción                      |
| -------- | -------------------------------- |
| eje_01.c | Hola mundo                       |
| eje_02.c | Tipos de variables               |
| eje_03.c | Condicionales                    |
| eje_04.c | Ciclos                           |
| eje_05.c | Funciones                        |
| eje_06.c | Parámetros del main: argv - argc |
| eje_07.c | Arreglos                         |
| eje_08.c | Structs                          |