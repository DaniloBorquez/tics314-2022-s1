#include <stdio.h>
#include <string.h>

struct alumno {
  char nombre[80];
  float nota;
};
int main(int argc, char *argv[]) {
  struct alumno a1;

  a1.nombre[0] = 'D';
  a1.nombre[1] = 'a';
  a1.nombre[2] = 'n';
  a1.nombre[3] = 'i';
  a1.nombre[4] = 'l';
  a1.nombre[5] = 'o';
  a1.nombre[6] = '\0';
  a1.nota = 4.5;

  printf("%s %f\n", a1.nombre, a1.nota);

  strcpy(a1.nombre, "Eduardo");
  printf("%s %f\n", a1.nombre, a1.nota);

  return 0;
}