#include <stdio.h>

int main(int argc, char *argv[]) {
  int ar[] = {1, 2, 3, 4, 5};
  int ar2[5];

  for (int i = 0; i < 5; i++) {
    printf("%i\n", ar[i]);
  }

  printf("\n");
  ar[2] = 10;
  for (int i = 0; i < 5; i++) {
    printf("%i\n", ar[i]);
  }

  for (int i = 0; i < 5; i++) {
    ar2[i] = i * 10;
  }
  for (int i = 0; i < 5; i++) {
    printf("%i\n", ar2[i]);
  }
  return 0;
}